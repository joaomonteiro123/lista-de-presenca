<HTML>
<HEAD>
    <TITLE>IIPC BH - Lista de Presença</TITLE>
    <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
</HEAD>


<BODY>
    <?php 
    include "navbar.php";
    ?>

    <div id="div_corpo">
        <?php 
        include "lista_de_presenca.php";
        ?>
    </div>

    <script src="popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="main.js"></script>  
</BODY>
