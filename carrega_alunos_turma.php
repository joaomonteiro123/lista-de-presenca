<?php

include "conectasql.php";

$nomes_filtro = "SELECT IFNULL(a.NOME,asi.nome) as aluno,
o.descricao as origem,
IFNULL(a.CODIGO,'-') as iipcnet,
IFNULL(IFNULL(a.telefone1,IFNULL(a.telefone2,a.telefone3)),IFNULL(asi.telefone_fixo,'-')) as telefone,
IFNULL(a.EMAIL,IFNULL(asi.email,'-')) as email,
IFNULL(a.rg,IFNULL(asi.rg,'-')) as rg,
IF(tga.fl_primeira_vez=1,'Sim','Nao') as primeira_vez
FROM turma_gratuita tg
INNER JOIN turma_gratuita_aluno tga ON tga.id_turma = tg.id_turma
LEFT JOIN aluno a ON a.CODIGO = tga.id_aluno
LEFT JOIN aluno_sem_iipcnet asi ON (asi.rg = tga.id_aluno OR asi.cpf = tga.id_aluno)
INNER JOIN tipo_origem o ON o.id = tga.tp_origem
WHERE tg.id_turma= '".$_POST['turma']."'";

$res_nomes = $conexao ->query($nomes_filtro);
$nomes = array();

$i = 0;
while ($n = $res_nomes -> fetch_assoc()) {
    $nomes[$i]["iipcnet"] = utf8_encode($n['iipcnet']);
    $nomes[$i]["aluno"] = utf8_encode($n['aluno']);
    $nomes[$i]["origem"] = utf8_encode($n['origem']);
    $nomes[$i]["telefone"] = utf8_encode($n['telefone']);
    $nomes[$i]["email"] = utf8_encode($n['email']);
    $nomes[$i]["rg"] = utf8_encode($n['rg']);
    $nomes[$i]["primeira_vez"] = utf8_encode($n['primeira_vez']);
    $i = $i + 1;
}


echo json_encode($nomes);

?>