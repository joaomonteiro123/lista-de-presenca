<?php

include "conectasql.php";

$nomes_filtro = "(SELECT CODIGO, NOME, IFNULL(CONCAT('CPF: ',cpf),IFNULL(CONCAT('RG: ' ,rg),IFNULL(IFNULL
(CONCAT('Nascimento: ' , DATA_NASCIMENTO), '-'), '-'))) as nasc, 0 as fl_chave_rg FROM `aluno`
WHERE LOWER(NOME) LIKE LOWER('".trim(strip_tags(utf8_decode($_GET['term'])))."%'))
UNION (SELECT IFNULL(rg,cpf) AS CODIGO, nome AS NOME,
IFNULL(CONCAT('CPF: ',cpf),IFNULL(CONCAT('RG: ' ,rg),IFNULL(CONCAT('Nascimento: ' ,data_nascimento),' - '))) AS nasc, 1 as fl_chave_rg
FROM `aluno_sem_iipcnet` WHERE LOWER(nome) LIKE LOWER('".trim(strip_tags(utf8_decode($_GET['term'])))."%'))
ORDER BY NOME LIMIT 10";

$res_nomes = $conexao ->query($nomes_filtro);
$nomes = array();

$i = 0;
while ($n = $res_nomes -> fetch_assoc()) {
    $nomes[$i]["id"] = utf8_encode($n['CODIGO']);
    $nomes[$i]["label"] = utf8_encode($n['NOME'])." - ".utf8_encode($n['nasc']);
    $nomes[$i]["value"] = utf8_encode($n['NOME'])." - ".utf8_encode($n['nasc']);
    $nomes[$i]["nasc"] = utf8_encode($n['nasc']);
    $nomes[$i]["fl_chave_rg"] = utf8_encode($n['fl_chave_rg']);
    $i = $i + 1;
}


echo json_encode($nomes);

?>