
<script>  
    $(document).ready(function () { 
        $("#link_turmas").click( function (){
            $("#link_lista").removeClass('active');
            $(this).addClass('active');
            $('#div_corpo').fadeOut('fast', function(){
                $('#div_corpo').load("consulta_turma.php", function(){
                    $('#div_corpo').fadeIn('fast');
                });
            });
        });

        $("#link_lista").click( function (){
            $("#link_turmas").removeClass('active');
            $(this).addClass('active');
            $('#div_corpo').fadeOut('fast', function(){
                $('#div_corpo').load("lista_de_presenca.php", function(){
                    $('#div_corpo').fadeIn('fast');
                });
            });
        });
    });
</script>

<nav class="navbar navbar-expand-md fixed-top navbar-dark" style="background-color: rgb(10, 136, 214);">
    <a class="navbar-brand" href="#">IIPC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active" id="link_lista">
                <a class="nav-link" style="cursor: pointer;">Lista de Presença<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item" id="link_turmas">
                <a class="nav-link" style="cursor: pointer;">Consultar Turmas</a>
            </li>
        </ul>
    </div>
</nav>