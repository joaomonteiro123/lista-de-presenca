<?php

include "conectasql.php";

$turma_filtro = "SELECT tg.nome as tema, t.DATA_INICIAL as data, v.nome as professor
FROM turma_gratuita tg
LEFT JOIN turma t ON t.CODIGO = tg.id_turma
INNER JOIN voluntario v ON v.codigo = tg.id_voluntario
WHERE tg.id_turma= '".$_POST['turma']."'";

$res_turma = $conexao ->query($turma_filtro);
$num_rows = $res_turma -> num_rows;

$turma = array();
$i = 0;
if ($num_rows == 0){
    $turma[$i]["fl_turma"] = 0;
}else{
    $turma[$i]["fl_turma"] = 1;
    while ($n = $res_turma -> fetch_assoc()) {
        $turma[$i]["tema"] = utf8_encode($n['tema']);
        $turma[$i]["data"] = date("d/m/Y", strtotime(utf8_encode($n['data'])));
        $turma[$i]["professor"] = utf8_encode($n['professor']);
        $i = $i + 1;
    }
}


echo json_encode($turma);

?>