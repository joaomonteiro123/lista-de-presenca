<script>  
    $(document).ready(function () { 

        $("#btn_detalhes").click(function (){
            $("#th_evento").html("");
            $("#tabela_alunos .tr_aluno").remove();
            $.ajax({
                url: "carrega_turma.php",
                type: "post",
                dataType: "json",
                data: "turma="+$("#turma").val(),
                success: function(turma){
                    if (turma[0]['fl_turma'] == 1){
                        $("#th_evento").html(turma[0]['tema']+" - "+turma[0]['data']+"</br>"+turma[0]['professor']);
                        $.ajax({
                            url: "carrega_alunos_turma.php",
                            type: "post",
                            dataType: "json",
                            data: "turma="+$("#turma").val(),
                            success: function(alunos){
                                for (var i = 0; i < alunos.length; i++) {
                                    coluna1 = "<td>"+alunos[i]['iipcnet']+"</td>";
                                    coluna2 = "<td>"+alunos[i]['primeira_vez']+"</td>";
                                    coluna3 = "<td>"+alunos[i]['aluno']+"</td>";
                                    coluna4 = "<td>"+alunos[i]['origem']+"</td>";
                                    coluna5 = "<td>"+alunos[i]['telefone']+"</td>";
                                    coluna6 = "<td>"+alunos[i]['email']+"</td>";
                                    coluna7 = "<td>"+alunos[i]['rg']+"</td>";
                                if (i % 2 === 0) {
                                    newRowContent = "<tr class='tr_aluno' style='background-color: #e8e8e8;'>"+coluna1+coluna2+coluna3+coluna4+coluna5+coluna6+coluna7+"</tr>";
                                 }
                                else {
                                    newRowContent = "<tr class='tr_aluno'>"+coluna1+coluna2+coluna3+coluna4+coluna5+coluna6+coluna7+"</tr>";                                
                                }
                                    $('#tabela_alunos tr:last').after(newRowContent);
                                }
                            }
                        })
                        
                        $('html, body').animate({
                            scrollTop: $('#table').offset().top - 100 //#DIV_ID is an example. Use the id of your destination on the page
                        }, 'slow');


                    }else{
                        alert("Turma não encontrada!");
                    }
                }
            })
        });

    });
</script>


<div class="py-5 text-center" style="margin-top:20px;">
    <img class="d-block mx-auto mb-4" src="Images/IIPC.png" alt="" width="200">
    <h2>Lista de Presença</h2>
    <p class="lead">Consultar turmas gratuitas</p>
</div>

<div class="container">
    <div class="row" style="text-align: left;">
        <div class="col-md-6">
            <label for="turma">Código da Turma</label>
        </div>
    </div>
    <div class="row mb-5" style="text-align: left;"> 
        <div class="col-md-3 mb-3">
            <input type="text" class="form-control" id="turma" name="turma">
            <small id="small_cpf" class="text-muted">Igual ao IIPC Net</small> 
        </div>
        <div class="col-md-3">                    
            <button id="btn_detalhes" class="btn btn-sm btn-info" style="height:35px;disabled:true">Consultar Turma</button>						
        </div>
    </div>

    <table class="table" style="font-size: small;" id="table">
        <thead class="thead-dark table-solid" style="text-align: center;">
            <tr><th id="th_evento"></th></tr>
        </thead>
    </table>
    <table class="table" style="font-size: small; border: 1px solid #375b5b" id="tabela_alunos">
        <thead class="thead-dark table-solid" style="text-align: center;">
            <tr>
                <th style="width:8%; text-align: center">IIPC Net</th>
                <th style="width:10%; text-align: center">Primeira vez?</th>
                <th style="width: 20%; text-align: center">Aluno</th>
                <th style="width: 10%; text-align: center">Origem</th>
                <th style="width: 15%; text-align: center">Telefone</th>
                <th style="width: 20%; text-align: center">E-mail</th>
                <th style="width: 10%; text-align: center">RG</th>
            </tr>
        </thead>
        <tbody style="text-align: center;"> 
        </tbody>
    </table>
</div>