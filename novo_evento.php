<?php 
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

$voluntarios_filtro = "SELECT DISTINCT v.* FROM voluntario v ORDER BY nome";
$res_voluntario = $conexao -> query($voluntarios_filtro);

$turma_filtro = "SELECT e.NOME, t.CODIGO FROM turma t INNER JOIN evento e ON e.id = t.ID_EVENTO WHERE
DATE(t.DATA_INICIAL) = DATE(NOW()) AND NOT EXISTS (SELECT 1 FROM turma_gratuita tg WHERE t.CODIGO = tg.id_turma)";
$res_turma = $conexao -> query($turma_filtro);
?>


<HTML>
<HEAD>
    <TITLE>IIPC BH - Eventos Gratuitos</TITLE>
    <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <link rel="stylesheet" href="estilo.css">
</HEAD>

<script>  
    $(document).ready(function () {
        $("#codigo_evento").change(function(){
            $("#tipo_evento").val($(this).find('option:selected').attr("name"));
            if (typeof $(this).find('option:selected').attr("name") != 'undefined'){
                $("#lbl_nome_evento").text("Qual o tema do(a) "+$(this).find('option:selected').attr("name")+"?");
            }else{
                $("#lbl_nome_evento").html('</br>');
            }
        });
    });
    function cadastrar_evento() {
        if ($("form")[0].checkValidity()) {
            $.ajax({
                url: "enviar_turma.php",
                type: "post",
                data: $("form").serialize(),
                success: function(){
                    window.location = "index.php";
                }
            })
        }else{
            alert("Preencha todos os campos obrigatórios!");
        } 
    }
</script>

<BODY>
    <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="Images/IIPC.png" alt="" width="200">
        <h2>Eventos Gratuitos</h2>
    </div>

    <div class="container">

        <div class="card-deck text-center">
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Informações do Evento</h4>
                </div>
                <div class="card-body"> 
                    <form id="frm_evento">                  
                        <div class="row">
                            <div class="col-md-3 mb-4" style="text-align:left">
                                <label for="codigo_evento">Código do Evento</label>
                                <div class="row">
                                    <select class="custom-select d-block w-100" name="codigo_evento" id="codigo_evento" required>
                                        <option value=''>Escolha...</option>
                                        <?php 
                                            while ($linha_turma = $res_turma -> fetch_assoc()){
                                                ?>
                                                <option name="<?=$linha_turma['NOME']?>" value="<?=$linha_turma['CODIGO']?>"><?=$linha_turma['CODIGO']?></option>                                      
                                                <?php 
                                            }  
                                        ?> 
                                    </select>
                                </div>
                                <div class="row"><font style="cursor: pointer; margin-left:5px" color="blue" id="myBtn">Código da turma não disponível?</font></div>
                            </div>
                            <div class="col-md-9 mb-4" style="text-align:left">
                                <label for="tipo_evento">Tipo de Evento</label>
                                <input disabled type="text" style="background-color: #f2f2f2;" class="form-control" name="tipo_evento" id="tipo_evento" placeholder="" value="" required>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-4 mb-4" style="text-align:left">
                                <label for="professor">Nome do professor</label>
                                <select class="custom-select d-block w-100" name="professor" id="professor" required>
                                    <option value=''>Escolha...</option>
                                    <option value='1'>Professor de fora</option>
                                    <?php 
                                        while ($linha_voluntario = $res_voluntario -> fetch_assoc()){
                                            ?>
                                            <option value="<?=$linha_voluntario['codigo']?>"><?=utf8_encode($linha_voluntario['nome'])?></option>  
                                            <?php 
                                        }  
                                    ?> 
                                </select>
                            </div>
                            <div class="col-md-8 mb-4" style="text-align:left">
                                <label for="nome_evento" id="lbl_nome_evento"></br></label>
                                <input type="text" style="background-color: white;" class="form-control" name="nome_evento" id="nome_evento" placeholder="" value="" required>
                                <small class="text-muted">Exemplos: "Sexta do livro: Teoria e prática da experiência fora do corpo", "Aula experimental do CIP", "Você já teve experiência fora do corpo?"</small>
                                <br/>
                            </div>       
                        </div>
                        <div class="row">
                            <div class="col-md-4"  style="text-align:left">
                                <button type="button" class="btn btn-md btn-primary" onClick="cadastrar_evento()">Cadastrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal">
        <div class="modal-content col-md-8 offset-2" style="padding-top: 0;">
            <div class="row">
                <div class="col-md-1 offset-11">
                    <span class="close">&times;</span>
                </div>
            </div>
            <?php
            include "nova_turma.php"
            ?> 
        </div>
    </div>

    <script src="popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script>
        var background = document.getElementById('myBackground');
        var modal = document.getElementById('myModal');
        var btn = document.getElementById("myBtn");
        var span = document.getElementsByClassName("close")[0];
        btn.onclick = function() {
            modal.style.display = "block";
        }
        span.onclick = function() {
            modal.style.display = "none";
        }
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
</BODY>