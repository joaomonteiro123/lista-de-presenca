<?php 
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

$origens_filtro = "SELECT DISTINCT o.* FROM tipo_origem o ORDER BY descricao";
$res_origem = $conexao -> query($origens_filtro);
?>

<script>  
    $(document).ready(function () { 
        $('input[type=radio][name=primeira_vez]').change(function() {
            if (this.value == 'sim') {
                $('#novo_aluno').show();
                $('#novo_aluno_alert').hide();
                $("#cpf").removeAttr("disabled");
                $("#telefone").removeAttr("disabled");
                $("#lbl_cpf").text("RG");
                $("#small_cpf").text("Somente letras e números");
                $("#cpf").css("background-color","white");
                $("#telefone").css("background-color","white");
                $("#cpf").val("");
                $("#aluno").val("");

                jQuery("#aluno").autocomplete("destroy");
                jQuery("#aluno").removeData('autocomplete');
            }
            else if (this.value == 'nao') {
                $('#novo_aluno').show();
                $('#novo_aluno_alert').show();
                $("#novo_aluno_alert").css("display","block");
                $("#cpf").val("");
                $("#aluno").val("");
                $("#aluno").autocomplete({
                    source: "listaaluno.php",
                    minLength: 3,
                    select: function (event, ui) {
                        $("#id_aluno").val(ui.item.id);;
                    },
                    change: function(event, ui) {
                        if (ui.item == null) {
                            alert("Selecione um aluno válido");
                            $("#aluno").val("");
                            $("#aluno").focus();
                            $("#cpf").val("");
                        }else{
                            $("#cpf").val(ui.item.nasc);
                            $("#fl_chave_rg").val(ui.item.fl_chave_rg);
                        }
                    }    
                });
                $("#lbl_cpf").html("<br/>");
                $("#small_cpf").text("");
                $("#cpf").attr("disabled",true);
                $("#cpf").css("background-color","#e5e5e5");
                $("#telefone").attr("disabled",true);
                $("#telefone").css("background-color","#e5e5e5");
            }
        });
    });

    function enviar_aluno(){
        if ($("form")[0].checkValidity()) {
            $.ajax({
                url: "enviar_aluno.php",
                type: "post",
                dataType: "json",
                data: $("form").serialize()+"&turma="+$("#turma_selecionada").val(),
                success: function(resultado){
                    if (resultado[0]['fl_sucesso'] == 0) {
                        alert(resultado[0]['erro']);
                    }else{
                        recarrega_telas();
                        $("body").animate(
                            {
                                scrollTop: 320 + "px"
                            }, 1000);
                    }
                }
            })
        }else{
            alert("Preencha todos os campos obrigatórios!");
        } 
    }
</script>

<div class="card-deck mb-3 text-center .tela_scroll" style="width: 100%;">
    <div class="card mb-4 box-shadow">
        <div class="card-header">
            <h4 class="my-0 font-weight-normal">Adicionar Aluno</h4>
        </div>
        <form id="frm_aluno">
            <div class="card-body">   
                <div class="row mb-3">
                    <div class="col-md-12" style="text-align:left;">
                        <label style="margin-right:15px" for="primeira_vez">Aluno de primeira vez?</label>
                        <div required class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-primary">
                                <input type="radio" name="primeira_vez" id="prmeira_vez_sim" value="sim" autocomplete="off">Sim
                            </label>
                            <label class="btn btn-primary">
                                <input type="radio" name="primeira_vez" id="prmeira_vez_nao" value="nao" autocomplete="off">Não
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row mb-3" style="display: none; text-align: left;" id="novo_aluno_alert">
                    <label style="color:red;margin-left:15px">Ao selecionar o nome do aluno, confira se o aluno é a mesmo para garantir que não são homônimos</label>
                </div>

                <div style="display: none;" id="novo_aluno">
                    <div class="row mb-4">
                        <div class="col-md-4"  style="text-align:left">
                            <label for="aluno" >Nome</label>
                            <input required type="text" style="background-color: white;" class="form-control input-lg" id="aluno" name="aluno">
                            <input type="hidden" id="id_aluno" name="id_aluno"/>
                        </div>
                        <div class="col-md-3"  style="text-align:left">
                            <label id="lbl_cpf" for="cpf"></label>
                            <input required maxlength="30" type="text" style="background-color: white;" class="form-control input-lg" id="cpf" name="cpf">
                            <small id="small_cpf" class="text-muted"></small>                        
                        </div>
                        <div class="col-md-2"  style="text-align:left">
                            <label for="telefone">Telefone</label>
                            <input type="text" style="background-color: white;" class="form-control input-lg" id="telefone" name="telefone">
                        </div>
                        <div class="col-md-3"  style="text-align:left">
                            <label for="origem" >Como ficou sabendo?</label>
                            <select class="custom-select d-block w-100" name="origem" id="origem" required>
                                <option value=''>Escolha...</option>
                                <?php 
                                    while ($linha_origem = $res_origem -> fetch_assoc()){
                                        ?>
                                        <option value="<?=$linha_origem['id']?>"><?=utf8_encode($linha_origem['descricao'])?></option>  
                                        <?php 
                                    }  
                                ?>
                            </select>
                        </div>
                        <input style="display: none" id="fl_chave_rg" name="fl_chave_rg"></input>
                    </div>
                    <div class="row">
                        <div class="col-md-4" style="text-align:left">
                            <button type="button" class="btn btn-md btn-primary" onClick='enviar_aluno()'>Cadastrar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> 