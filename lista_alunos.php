<script>
$(document).ready(function () {
    $.ajax({
        url: "busca_alunos.php",
        type: "post",
        dataType: "json",
        data: "turma="+$("#turma_selecionada").val(),
        success: function(alunos){
            for (var i = 0; i < alunos.length; i++) {
                newRowContent = "<tr id="+alunos[i]['CODIGO']+"><td>"+alunos[i]['NOME']+"</td><td>"+alunos[i]['CODIGO']+"</td><td>"+alunos[i]['descricao']+"</td><td><button class='btn btn-danger' id='btn_remover"+i+"'>Remover aluno</button></td><td style='display:none;'>"+alunos[i]['fl_rg']+"</td></tr>";
                $('#tbl_alunos tr:last').after(newRowContent);
                $("#btn_remover"+i).click( function (){
                    $.ajax({
                        url: "apaga_aluno.php",
                        type: "post",
                        data: "turma="+$("#turma_selecionada").val()+"&aluno="+$(this).parent().parent().find('td:nth-child(2)').text()+"&fl_rg="+$(this).parent().parent().find('td:nth-child(5)').text(),
                        success: function(){
                            recarrega_telas();
                        }
                    })
                });
            }
        }
    })
});
</script>

<div class="card-deck mb-3 text-center" style="width: 100%;">
    <div class="card-body">             
        <table class="table table-bordered table-hover" style="font-size: small" id="tbl_alunos">
            <thead style="text-align: center">
                <tr>
                    <th>Nome</th>
                    <th>RG ou IIPC Net</th>
                    <th>Como ficou sabendo?</th>
                    <th></th>
                </tr>
            </thead>
            <tbody style="text-align: center">
            <tbody>
        </table> 
    </div> 
</div>
