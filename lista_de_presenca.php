<?php 
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

$origens_filtro = "SELECT DISTINCT o.* FROM tipo_origem o ORDER BY descricao";
$res_origem = $conexao -> query($origens_filtro);

$eventos_filtro = "SELECT t.CODIGO, tg.nome as titulo, e.NOME as evento FROM turma_gratuita tg INNER JOIN turma t
ON DATE(t.DATA_INICIAL) = DATE(NOW()) AND t.CODIGO = tg.id_turma INNER JOIN evento e ON e.id = t.ID_EVENTO";
$res_eventos = $conexao -> query($eventos_filtro);
?>

<script>  
    $(document).ready(function () { 

        $(".bt_eventos").click( function (){
            $("#novos_alunos").load("novo_aluno.php");
            $("#alunos").load("lista_alunos.php");
            $(".bt_eventos").css("background-color","#868e96");
            $(this).css("background-color","#0a88d6");
            $(this).css("hover","#55595c");
            $(".bt_eventos").css("hover","#55595c");
            $("#turma_selecionada").val($(this).attr('value'));
            $("body").animate(
                {
                    scrollTop: 320 + "px"
                }, 1000);
        });

        $("#bt_novoevento").click( function (){
            window.location = "novo_evento.php";
        });
    });
     
    function recarrega_telas(){
        $("#novos_alunos").load("novo_aluno.php");
        $("#alunos").load("lista_alunos.php");
        
    }
</script>

<div class="py-5 text-center" style="margin-top:20px;">
    <img class="d-block mx-auto mb-4" src="Images/IIPC.png" alt="" width="200">
    <h2>Lista de Presença</h2>
    <p class="lead">Eventos gratuitos</p>
</div>

<div class="container">
    <input type="hidden" id="turma_selecionada" name="turma_selecionada"/>
    <div class="row mb-4">
        <?php 
            while ($linha_eventos = $res_eventos -> fetch_assoc()){
                ?>
                <div class="col-md-3">
                    <div class="card mb-4 box-shadow">
                        <button class="bt_eventos btn btn-secondary" value=<?=utf8_encode($linha_eventos['CODIGO'])?>  align="center" style="white-space: normal; hover:#55595c; padding: 7%; display: flex; align-items: center; height: 125px; width: 100%; text-align: center;">
                            <?=utf8_encode($linha_eventos['titulo'])?>
                        </button>
                    </div>
                </div> 
                <?php 
            }  
        ?>
        <div class="col-md-3">
            <div class="card mb-4 box-shadow">
                <button id="bt_novoevento" class="btn btn-light"  style="font-weight: bold; border: 2px solid #55595c; border-radius: 5px; height: 125px; text-align: center; line-height: 125px; ">
                    + Novo Evento
                </button>
            </div>
        </div>
    </div>
    <div class="row" id="novos_alunos"></div>
    <div class="row" id="alunos"></div>
</div>