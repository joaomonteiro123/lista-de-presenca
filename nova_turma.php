<?php 
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

$evento_filtro = "SELECT * FROM evento";
$res_evento = $conexao -> query($evento_filtro);
?>

<HTML>
<HEAD>
    <TITLE>Criar Turma</TITLE>
    <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
</HEAD>

<script>
function criarTurma() {
    if ($("#frm_turma")[0].checkValidity()) {
        $.ajax({
            url: "criar_turma.php",
            type: "post",
            data: $("#frm_turma").serialize(),
            success: function(){
                alert("Turma inserida com sucesso!");
                location.reload();
            }
        })
    }else{
        alert("Preencha todos os campos obrigatórios!");
    } 
    }
</script>

<BODY>
    <div class="py-3 text-center">
        <h2>Nova Turma</h2>
    </div>
    <div class="container">
        <form id="frm_turma">
            <div class="row">
                <div class="col-md-3 mb-4" style="text-align:left">
                    <label for="codigo_turma">Código da Turma</label>
                    <input type="text" class="form-control" name="codigo_turma" id="codigo_turma" style="background-color: white;" placeholder="" value="" required>
                    <small class="text-muted"><font color="red"><b>Igual ao IIPC Net</b></font></small>
                </div>
                <div class="col-md-9 mb-4" style="text-align:left">
                    <label for="tipo_evento">Tipo de Evento</label>
                    <select class="custom-select d-block w-100" name="evento" id="codigo_evento" required>
                        <option value=''>Escolha...</option>
                        <option value='3'>PPG</option>
                        <option value='75'>Aula Experimental</option>
                        <option value='75'>Café Consciência</option>
                        <option value='17'>Vídeo Projeção</option>
                        <option value='2701'>Sexta de Livro</option>
                        <option disabled>────────────────────────────────────────────────────────────</option>
                        <?php 
                            while ($linha_evento = $res_evento -> fetch_assoc()){
                                ?>
                                <option value="<?=$linha_evento['id']?>"><?=UTF8_ENCODE($linha_evento['NOME'])?></option>                                      
                                <?php 
                            }  
                        ?> 
                    </select>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-4"  style="text-align:left">
                <button type="button" class="btn btn-md btn-primary" onClick="criarTurma()">Cadastrar</button>
            </div>
        </div>
    </div>
</BODY>


    <script src="popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="main.js"></script>  
</BODY>